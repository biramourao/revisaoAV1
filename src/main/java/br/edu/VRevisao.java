package br.edu;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class VRevisao extends JFrame{

	private static final long serialVersionUID = 1L;
	
	JLabel lbNome;
	JLabel lbIdade;
	JLabel lbSexo;
	JTextField tfNome;
	JTextField tfIdade;
	JComboBox<String> cbSexo;
	JButton btCadastrar;
	JButton btLimpar;
	String optSexo[] = {"Masculino", "Feminino"};
	JPanel form1;
	JPanel form2;
	JPanel botoes;
	
	public VRevisao(){
		//Define um titulo
		super("Revisao AV1");
		//Define o tamanho da janela
		setSize(300, 150);
		//Define a centralização da criação da janela
		setLocationRelativeTo(null);
		//Define a operação de fechar
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		//Instancia os elementos da tela
		lbNome = new JLabel("Nome: ");
		lbIdade = new JLabel("Idade: ");
		lbSexo = new JLabel("Sexo: ");
		tfNome = new JTextField(15);
		tfIdade = new JTextField(3);
		cbSexo = new JComboBox<String>(optSexo);
		btLimpar = new JButton("Limpar");
		btCadastrar = new JButton("Cadastrar");
		
		//Define o layout principal
		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		
		//Instancia os paineis e "coloca os elementos" dentro
		form1 = new JPanel();
		form1.setLayout(new GridLayout(2, 2, 5, 5));
		form1.add(lbNome);
		form1.add(lbIdade);
		form1.add(tfNome);
		form1.add(tfIdade);
		
		form2 = new JPanel();
		form2.setLayout(new GridLayout(2, 1, 5, 5));
		form2.add(lbSexo);
		form2.add(cbSexo);
		
		botoes = new JPanel();
		botoes.setLayout(new GridLayout(1, 2 ,5 ,5));
		botoes.add(btLimpar);
		botoes.add(btCadastrar);
		botoes.setSize(300,80);
		
		//adiciona os paineis ao container principal
		add(form1);
		add(form2);
		add(botoes);
		
		//adiciona ação ao botao (adicionando ActionListener ao botao e instanciando anonimamente um ActionListener)
		btCadastrar.addActionListener(new ActionListener() {
			
			//adiciona ação ao botao (adicionando um metodo actionPerformed e dentro dele como parametro um ActionEvent e)
			public void actionPerformed(ActionEvent e){
				
				//circuncidar com try e catch a execucao da consulta
				try {
					//chama o metodo de cadastro e guarda o retorno
					int resultado = new RevisaoDAO().cadastrar(tfNome.getText(), tfIdade.getText(), cbSexo.getSelectedItem().toString());
					//verifica se o cadastro deu certo e exibe a mensagem
					if (resultado == 1) {
						System.out.println("Cadastro efetuado com sucesso!");
					}
				} catch (SQLException e1) {
					//mostra a mensagem de erro, caso o cadastro der errado.
					System.out.println("Erro ao cadastrar!\n Detalhes: " + e1.getMessage());
					e1.printStackTrace();
				}		
			}
		});
		//Adiciona ação ao botao ou seja adiciona um ActionListener
		btLimpar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//limpa os campos
				tfNome.setText("");
				tfIdade.setText("");
			}
		});
		//deixa a janela visível
		setVisible(true);
	}
	public static void main(String[] args) {
		new VRevisao();
	}
}
