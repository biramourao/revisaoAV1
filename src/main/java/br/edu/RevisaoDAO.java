package br.edu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RevisaoDAO{

	//metodo de cadastro que receberá as informações inseridas na tela VRevisao
	//esse metodo tem que ser o throws SQLException para tratar excessões relacionadas ao BD
	public int cadastrar(String nome, String idade, String sexo) throws SQLException {
		//Vou descrever os passos para criar um metodo que faça inserção no banco:
		
		//1 - Abrir conexao
		Connection con = Conexao.getConnection();
		
		//2 - Preparar a consulta 
		//	{criar o PreparedStatement e atribuir um prepareStatement que vem da conexao}
		// reparem que o primeiro valor da minha inserção é null, porque? é que na minha tabela cliente
		// o id é auto incremento ou seja preenche sozinho se receber um valor null #ficaADica
		PreparedStatement pStatement = con.prepareStatement("INSERT INTO cliente VALUES(null,?,?,?)");
		
		//3 - Setar os valores no PreparedStatement criado no passo anterior
		pStatement.setString(1, nome);
		pStatement.setInt(2, Integer.parseInt(idade));
		pStatement.setString(3, sexo);
		
		//4 - Executar a query e pegar o resultado com uma variavel int.
		//	* Não é obrigado a pegar o resultado com uma variavel poderá somente executar a query 
		// 	  pStatement.executeUpdate();
		int resultado = pStatement.executeUpdate();
		
		//5 - fechar o PreparedStatement (consulta);
		pStatement.close();
		
		//6 - fechar a conexao
		con.close();
		
		// 7 - retornar o resultado
		// * Não é obrigado a retornar o resultado esse metodo poderia ser (Void) gosto de retornar pois
		//	 deixamos o usuário informado sobre a situação do seu cadastro, se deu certo ou não.
		return resultado;
	}
	
}
