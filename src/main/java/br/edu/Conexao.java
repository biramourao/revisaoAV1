package br.edu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexao {
	
	//costumo dividir os parametro em variaveis para um melhor entendimento
	
	//variavel que guarda o usuário do BD
	private static String user = "root";
	//variavel que guarda a senha do BD
	private static String password = "root";
	//variavel que guarda a URL(localização) do BD
	private static String location = "jdbc:mysql://localhost:3306/revisao";
	//Variavel que recebera a conexao
	private static Connection con = null;
	
	//metodo que cria de fato a conexão esse metodo tem que ser public e static 
	//para que seja acessado em qualquer lugar do programa
	//e sem a necessidade de instanciar a classe de conexao é o somente necessário chamá-la 
	//da seguinte forma: Conexao.getConnection(); que irá receber a conexao.
	public static Connection getConnection() throws SQLException {
		//verifica se a conexao está nula para que crie uma nova
		if (con == null) {
			//se estiver nula o metodo criará uma conexão nova.
			con = DriverManager.getConnection(location, user, password);
		}else {
			//se não estiver nula, ele fechará a conexão atual e criará uma nova para minimizar erros.
			con.close();
			con = DriverManager.getConnection(location,user,password);
		}
		//retorna a conexao criada
		return con;
	}

}
